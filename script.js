function currentTime(start, speed) {
    const date = new Date(start + speed*(Date.now() - start))
    const hour = date.getHours()*3600
    const minute = date.getMinutes()*60
        + date.getSeconds()
        + date.getMilliseconds()/1000
    return [date, (hour+minute)/86400, minute/3600]
}

function update(displayContainer, start, speed, scaleH, scaleM) {
    let time = currentTime(start, speed)
    let cHour = scaleH(time[1])
    let cMinute = scaleM(time[2])
    displayContainer.style.background = `linear-gradient(135deg, ${cHour} 40%, ${cMinute} 60%)`
    displayContainer.title = time[0].toTimeString()
}

function main() {
    const displayContainer = document.getElementById('display')
    const start = Date.now()

    const params = new URL(window.location.href).searchParams
    const speed = parseFloat(params.get('speed')) || 1
    const fps = parseFloat(params.get('fps')) || 5

    const scaleH = chroma.scale(colorsH)
    const scaleM = chroma.scale(colorsM)

    window.setInterval(() => update(displayContainer, start, speed, scaleH, scaleM), 1000/fps)
}

const colorsH = ['#303b54', '#ffd500', '#7dd8ff', '#ee6c31', '#000000']
const colorsM = ['#303b54', '#00c951']

window.addEventListener('load', main)
